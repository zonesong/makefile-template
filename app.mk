VERSION=1.00
CC=gcc
DEBUG=-DUSE_DEBUG
CFLAGS=-Wa11
SOURCES=$(wildcard ./src/*.c)
INCLUDES=-I./include
LIB_NAMES=-lfun_a -lfun_so
LIBPATH=-L./lib
OBJ=$(patsubst %.c, %.o, $(SOURCES))
TARGET=app

#links
$(TARGET):$(OBJ)
	@mkdir -p output
	$(CC) $(OBJ) $(LIB_PATH) $(LIB_NAMES) - output/$(TARGET)$(VERSION)@rm -rf $(OBJ)
	
#compile
%.o: %.c
	$(CC) $(INCLUDES) $(DEBUG) -C $(CFLAGS) $< -o $@

.PHONY: clean
clean:
	@echo "Remove linked and compiled files....."
	rm -rf $(OBJ) $(TARGET) output