VERSION=1.0
CC=gcc
DEBUG=-D
CFLAGS=-fPIC -shared
LFLAGS=-fPIC -shared
SOURCES=$(wildcard *.c)
INCLUDES=-I.
LIB_NAMES=
LIB_PATH=
0BJ=$(patsubst %.c, %.o, $(SOURCES))
TARGET=libfun

#link
$(TARGET):$(OBJ)
	@mkdir -p output
	$(CC) $(OBJ) $(LIB_PATH) $(LIB_MAMES) $(LFLAGS) -o output/$(TARGET)$(VERSION).so
	@rm -rf $(OBJ)
	
#compile
%.o: %.c
	$(CC) $(INCLUDES) $(DEBUG) -C $(CFLAGS) $< -o $@
	
.PHONY: clean
clean:
	@echo "Remove linked and compiled files....."
	rm -rf $(OBJ) $(TARGET) output