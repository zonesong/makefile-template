VERSION=1.0
CC=gcc
DEBUG=-DUSE_DEBUG
CFLAGS=-Wa11
AR=ar
ARFLAGS=rv
SOURCES=$(wildcard *.c)
INCLUDES=-I./include
LIB_NAMES=-lfun_a -lfun_so
LIBPATH=-L./lib
0BJ=$(patsubst %.c, %.o, $(SOURCES))
TARGET=libfun

#link
$(TARGET):$(OBJ)
	@mkdir -p output
	$(AR) $(ARFLAGS) output/$(TARGET)S(VERSION).a $(OBJ)
	@rm -rf $(OBJ)
	
#compile
%.o: %.c
	$(CC) $(INCLUDES) $(DEBUG) -C $(CFLAGS) $< -o $@
	
.PHONY: clean
clean:
	@echo "Remove linked and compiled files ......"
	rm -rf $(OBJ) $(TARGET) output